local helpers = require("lain.helpers")
local shell   = require("awful.util").shell
local wibox   = require("wibox")
local string  = { match  = string.match,
                  format = string.format }

local function factory(args)
    local netman     = { widget = wibox.widget.textbox() }
    local args     = args or {}
    local timeout  = args.timeout or 10
    local settings = args.settings or function() end

    netman.cmd = args.cmd or "nmcli"
    netman.wDevice = args.wDevice or "wlp3s0"

    local format_cmd = string.format("%s device show %s",
                                     netman.cmd, netman.wDevice)

    netman.laststastus = {}

    function netman.update()
        helpers.async(format_cmd, function(conStatus)
                        local s = string.match(conStatus,
                                                 "GENERAL.STATE:%s+(%d+)")
            if netman.laststatus ~= s then
              netstatus = { state = s }
              widget = netman.widget
              settings()
              netman.laststatus = netStatus
            end
        end)
    end

    helpers.newtimer(string.format("netman-%s-%s", netman.cmd, netman.wDevice), timeout, netman.update)

    return netman
end

return factory
